PET API
=======

## INSTALL

### Prerequisites
- PHP
- MySQL

### Tool Prerequisites
- [composer](https://getcomposer.org/)

### Steps
- Clone this
- Edit database configuration at 
    ```
        ../application/config/database.php
    ```
- Open terminal and run this
	```
		sh composer.sh && sh install.sh
	```

## API CALL

### Create
```
Endpoint: /pet
Method: POST
Content-Type: application/json
Body (raw):
{
    "name": "Kitty",
    "age": 9
}

Return Value:
{
    "status": "Success",
    "data": {
        "name": "Kitty",
        "age": 9
    }
}
```

### Read
```
Endpoint: /pet/{pet_id}
Method: GET
Content-Type: none (unnecessary)

Return Value:
{
    "status": "Success",
    "data": {
        "id": "1",
        "name": "Kitty",
        "age": "7",
        "photo": "https://jurnalpublik.com/dwisetiyadi/kudo/uploads/e80acce018c38bca366dfff20bb284c4.jpg"
    }
}
```

### Update
```
Endpoint: /pet/{pet_id}
Method: PUT
Content-Type: application/json
Body (raw):
{
    "name": "Kitty",
    "age": 9
}

Return Value:
{
    "status": "Success",
    "data": {
        "name": "Kitty",
        "age": 9
    }
}
```

### Delete
```
Endpoint: /pet/{pet_id}
Method: DELETE
Content-Type: none (unnecessary)

Return Value:
{
    "status": "Success",
    "data": "";
}
```

### Upload Photo
```
Endpoint: /pet/{pet_id}/uploadImage
Method: POST
Content-Type: none (unnecessary)
Form Data (input-name): photo 

Return Value:
{
    "status": "Success",
    "data": {
        "file_name": "4352ea6767e8422043c7379c0f161165.jpg",
        "file_type": "image/jpeg",
        "file_path": "/../kudo/uploads/",
        "full_path": "/../kudo/uploads/4352ea6767e8422043c7379c0f161165.jpg",
        "raw_name": "4352ea6767e8422043c7379c0f161165",
        "orig_name": "15302973_G-696x464.jpg",
        "client_name": "15302973_G-696x464.jpg",
        "file_ext": ".jpg",
        "file_size": 21.79,
        "is_image": true,
        "image_width": 696,
        "image_height": 464,
        "image_type": "jpeg",
        "image_size_str": "width=\"696\" height=\"464\""
    }
}
```