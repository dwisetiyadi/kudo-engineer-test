<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Install extends CI_Controller
{
	public function index()
	{
	    $this->load->model('pets');
	    $rv = $this->pets->install();
	    if ($rv)
	    {
	        echo 'Install table Pet succed' . "\n";
	    }
	    else
	    {
	        echo 'Something goes wrong!' . "\n";
	    }
		
	}
}
