<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pet extends CI_Controller
{
    public function index($id = '', $upload = '')
    {
        switch ($this->input->method(TRUE)) {
        case 'POST':
            $this->create($id, $upload);
            break;
        case 'GET':
            $this->read($id);
            break;
        case 'PUT':
            $this->update($id);
            break;
        case 'DELETE':
            $this->delete($id);
            break;
        }
    }
    
	private function create($id, $upload)
	{
	    if ($id != '' && $upload == 'uploadImage') {
	        $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 200;
            $config['encrypt_name']         = TRUE;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('photo'))
            {
                $return['status'] = 'Error';
                if ($this->input->method(TRUE) == 'POST') {
                    $return['data'] = $this->upload->display_errors('', '');
                }
                else
                {
                    $return['data'] = 'Not POST Request';
                }
            }
            else
            {
                $this->load->helper('url');
                $data['photo'] = base_url('uploads/' . $this->upload->data('file_name'));
                $this->load->model('Pets');
                $this->Pets->update($id, $data);
                
                $return['status'] = 'Success';
                $return['data'] = $this->upload->data();
            }
	    }
	    else
	    {
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            
            $input = json_decode($this->input->raw_input_stream);
            
            if (json_last_error() === JSON_ERROR_NONE && $input != '') {
        	    $set_data['name'] = $input->name;
        	    $set_data['age'] = $input->age;
            }
            else
            {
                $set_data['name'] = '';
        	    $set_data['age'] = '';
            }
            
    	    $this->form_validation->set_data($set_data);
    	    $this->form_validation->set_rules('name', 'Pet Name', 'trim|required');
    	    $this->form_validation->set_rules('age', 'Pet Age', 'numeric|required');
    	    
    	    if ($this->form_validation->run() == FALSE)
            {
                $return['status'] = 'Error';
                if ($this->input->method(TRUE) == 'POST') {
                    $return['data'] = $this->form_validation->error_array();
                }
                else
                {
                    $return['data'] = 'Not POST Request';
                }
            }
            else
            {
                $data['name'] = $input->name;
                $data['age'] = $input->age;
                $this->load->model('Pets');
                $this->Pets->create($data);
                
                $return['status'] = 'Success';
                $return['data'] = $data;
            }
	    }
        
        $this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return))
			->_display();

		exit();
	}
	
	private function read($id = '')
	{
        $this->load->model('Pets');
        $data = $this->Pets->read($id);
        
        if ($data)
        {
            $return['status'] = 'Success';
            $return['data'] = $data;
        }
        else
        {
            $return['status'] = 'Error';
            $return['data'] = '';
        }
        
        $this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return))
			->_display();

		exit();
	}
	
	private function update($id = '')
	{
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        
        $input = json_decode($this->input->raw_input_stream);
        
        if (json_last_error() === JSON_ERROR_NONE && $input != '') {
    	    $set_data['name'] = $input->name;
    	    $set_data['age'] = $input->age;
        }
        else
        {
            $set_data['name'] = '';
    	    $set_data['age'] = '';
        }
	    
	    $this->form_validation->set_data($set_data);
	    $this->form_validation->set_rules('name', 'Pet Name', 'trim|required');
	    $this->form_validation->set_rules('age', 'Pet Age', 'numeric|required');
	    
	    if ($this->form_validation->run() == FALSE)
        {
            $return['status'] = 'Error';
            if ($this->input->method(TRUE) == 'PUT') {
                $return['data'] = $this->form_validation->error_array();
            }
            else
            {
                $return['data'] = 'Not POST Request';
            }
        }
        else
        {
            $data['name'] = $input->name;
            $data['age'] = $input->age;
            $this->load->model('Pets');
            $this->Pets->update($id, $data);
            
            $return['status'] = 'Success';
            $return['data'] = $data;
        }
        
        $this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return))
			->_display();

		exit();
	}
	
	private function delete($id = '')
	{
        $this->load->model('Pets');
        $data = $this->Pets->delete($id);
        
        $return['status'] = 'Success';
        $return['data'] = '';
        
        $this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return))
			->_display();

		exit();
	}
}
