<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pets extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }
    
    public function install()
    {
        $rv = $this->db->query('CREATE TABLE IF NOT EXISTS `pet` (`id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(100) NOT NULL, `age` int(11) NOT NULL, `photo` tinytext, PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;');
        return $rv;
    }

    public function create($data)
    {
        $this->db->insert('pet', $data);
    }

    public function read($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('pet');
        return $query->row();
    }

    public function update($id, $data)
    {
        $this->db->update('pet', $data, array('id' => $id));
    }

    public function delete($id)
    {
        $this->db->delete('pet', array('id' => $id));
    }
}